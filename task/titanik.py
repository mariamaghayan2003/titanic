import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    titles = ["Mr.", "Mrs.", "Miss."]
    res = []
    for title in titles:
        title_df = df[df['Name'].str.contains(title)]
        missing = title_df['Age'].isnull().sum()
        med_age = round(title_df['Age'].median())
        res.append((title, missing, med_age))
    return res
